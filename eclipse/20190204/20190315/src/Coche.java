
public class Coche extends Vehiculo {
	
	char tipo;
	int bastidor;
	int ruedas;
	boolean descapotable;
	
	//Metodo Constructor para crear objetos Coche
	public Coche(String matricula, String marca, String modelo, double precio, char tipo, int bastidor, int ruedas,
			boolean descapotable) {
		super(matricula, marca, modelo, precio); //Las trae del "PADRE"
		this.tipo = tipo;
		this.bastidor = bastidor;
		this.ruedas = ruedas;
		this.descapotable = descapotable;
	}

	
	//Metodos get & set
	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public int getBastidor() {
		return bastidor;
	}

	public void setBastidor(int bastidor) {
		this.bastidor = bastidor;
	}

	public int getRuedas() {
		return ruedas;
	}

	public void setRuedas(int ruedas) {
		this.ruedas = ruedas;
	}

	public boolean isDescapotable() {
		return descapotable;
	}

	public void setDescapotable(boolean descapotable) {
		this.descapotable = descapotable;
	}


	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", tipo=" + tipo + ", bastidor=" + bastidor + ", ruedas=" + ruedas + ", descapotable=" + descapotable
				+ "]";
	}	
}
