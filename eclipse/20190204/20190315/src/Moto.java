
public class Moto extends Vehiculo {
	
	//Atributos de la clase moto
	int cilindrada;
	int numeroLuces;
	boolean radio;
	
	//Metodo Constructor
	public Moto(String matricula, String marca, String modelo, double precio, int cilindrada, int numeroLuces,
			boolean radio) {
		super(matricula, marca, modelo, precio);
		this.cilindrada = cilindrada;
		this.numeroLuces = numeroLuces;
		this.radio = radio;
	}

	public int getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}

	public int getNumeroLuces() {
		return numeroLuces;
	}

	public void setNumeroLuces(int numeroLuces) {
		this.numeroLuces = numeroLuces;
	}

	public boolean isRadio() {
		return radio;
	}

	public void setRadio(boolean radio) {
		this.radio = radio;
	}

	
	//Metodo ToString
	@Override
	public String toString() {
		return "Moto [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", cilindrada=" + cilindrada + ", numeroLuces=" + numeroLuces + ", radio=" + radio + "]";
	}
	
	

	
	
	
}
