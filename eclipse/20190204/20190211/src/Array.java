
public class Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Array conjunto de datos del mismo tipo
		
		
		int [] conjunto = {8, 3, 5, 3, 17, 22, 0, 1, 9};
		//				   0  1  2  3  4    5  6  7  8
		// está formado por.. 9 numeros..
		
		System.out.println(conjunto[4]);
		System.out.println("El array esta formado por "+conjunto.length+ " numeros");
		System.out.println(conjunto[0]);
		System.out.println(conjunto[conjunto.length-1]); //Para sacar el ultimo numero del array
		System.out.println(conjunto);
		for (int i=0; i<conjunto.length; i++) {
			System.out.println("El array tiene: "+conjunto[i]);
		}
		
		char [] letras = {'g', 'T', 'o', 'w', '@', '5', '%', '$', 'F'};
		
		for (int i=0; i<letras.length; i++) {
			System.out.println(letras[i]);
		}
		/*
		String [] palabras = {"casa", "PC", "Perro", "tElE", "sol", "AudiA3"};
		
		palabras[2]= "Gato";
		palabras[2]= "Pez";
		
		for (int i=0; i<palabras.length; i++) {
			System.out.print(palabras[i]+"\t"); //print vacio
		}
		*/
		
		double [] numeros = {34.7, 2.6, 9.5, 23.0, 12, 67.8, 127.43};
		
		for (int i=numeros.length-1; i>=0; i--) {
			System.out.print(numeros[i]+"\t");
		}
				
	}

}
