public class Cuadrado extends Figura{
	
	private double lado;

	public Cuadrado(String color, char tamaño, double lado) {
		super(color, tamaño);
		this.lado = lado;
	}

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	@Override
	public String toString() {
		return "Cuadrado [color=" + color + ", tamaño=" + tamaño + ", lado=" + lado + "]";
	}
	
	public double calcularArea () {
		double area = getLado()*getLado();
		return area;

	}

}
