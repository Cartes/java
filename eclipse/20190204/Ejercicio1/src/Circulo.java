
public class Circulo extends Figura {
	
	private double radio;

	public Circulo(String color, char tamaño, double radio) {
		super(color, tamaño);
		this.radio = radio;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	@Override
	public String toString() {
		return "Circulo [color=" + color + ", tamaño=" + tamaño + ", radio=" + radio + "]";
	}
	
	
	public double calcularArea () {
		return Math.PI * Math.pow(getRadio(), 2);
				  
	}
	
	

}
