import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import java.awt.Cursor;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JPasswordField;
import javax.swing.DropMode;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Toolkit;
import com.toedter.calendar.JCalendar;
import javax.swing.border.CompoundBorder;
import com.toedter.calendar.JDateChooser;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GUI {

	//Un Jframe representa la ventana en su totalidad
	private JFrame frmEncuesta; 
	private JTextField txtIntroduzcaSuNombre;
	private final ButtonGroup buttonGroup = new ButtonGroup(); // Con esto crea los grupos de los radioButton
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JList list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmEncuesta.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEncuesta = new JFrame();
		frmEncuesta.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\prueba\\Pictures\\Saved Pictures\\wow.jpg"));
		frmEncuesta.setTitle("Encuesta");
		frmEncuesta.setBounds(100, 100, 823, 661); //Cambiamos la posicion y el tamaño X | Y | Ancho | Largo
		frmEncuesta.setLocationRelativeTo(null); //Sirve para centrar el contenido
		frmEncuesta.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEncuesta.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setEnabled(true);
		btnNewButton.setBounds(323, 205, 89, 23);
		frmEncuesta.getContentPane().add(btnNewButton);
		
		txtIntroduzcaSuNombre = new JTextField();
		txtIntroduzcaSuNombre.setToolTipText("Introduzca su Nombre");
		txtIntroduzcaSuNombre.setBounds(234, 135, 178, 23);
		frmEncuesta.getContentPane().add(txtIntroduzcaSuNombre);
		txtIntroduzcaSuNombre.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Introduzca Usuario:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNewLabel.setBounds(92, 139, 138, 14);
		frmEncuesta.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Dia a recordar");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 13));
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\CFGS\\Pictures\\Saved Pictures\\descarga.jpg"));
		lblNewLabel_1.setBounds(0, 453, 412, 169);
		frmEncuesta.getContentPane().add(lblNewLabel_1);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Eres del bar\u00E7a");
		chckbxNewCheckBox.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		chckbxNewCheckBox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckbxNewCheckBox.setBounds(166, 205, 141, 23);
		frmEncuesta.getContentPane().add(chckbxNewCheckBox);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Alumno");
		buttonGroup_1.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(139, 347, 109, 23);
		frmEncuesta.getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Profesor");
		buttonGroup_1.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(139, 373, 109, 23);
		frmEncuesta.getContentPane().add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.setBounds(24, 347, 109, 23);
		frmEncuesta.getContentPane().add(rdbtnNewRadioButton_2);
		
		JRadioButton rdbtnNewRadioButton_3 = new JRadioButton("Mujer");
		buttonGroup.add(rdbtnNewRadioButton_3);
		rdbtnNewRadioButton_3.setBounds(24, 373, 109, 23);
		frmEncuesta.getContentPane().add(rdbtnNewRadioButton_3);
		
		JLabel lblNewLabel_2 = new JLabel("Seleccione ciudad:");
		lblNewLabel_2.setBounds(24, 403, 122, 23);
		frmEncuesta.getContentPane().add(lblNewLabel_2);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setMaximumRowCount(3);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"MADRID", "BARCELONA", "VALENCIA", "SEVILLA", "BILBAO", "ZARAGOZA", "ALBACETE"}));
		comboBox.setSelectedIndex(1);
		comboBox.setBounds(137, 404, 103, 20);
		frmEncuesta.getContentPane().add(comboBox);
		
		JLabel lblObservaciones = new JLabel("Observaciones:");
		lblObservaciones.setBounds(591, 429, 109, 23);
		frmEncuesta.getContentPane().add(lblObservaciones);
		
		JPasswordField passwordField = new JPasswordField();
		passwordField.setEnabled(false);
		passwordField.setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
		passwordField.setToolTipText("Introduzca Password");
		passwordField.setEchoChar('*');
		passwordField.setBounds(234, 160, 178, 23);
		frmEncuesta.getContentPane().add(passwordField);
		
		JLabel lblIntroduzcaConstrasea = new JLabel("Constrase\u00F1a:");
		lblIntroduzcaConstrasea.setBounds(153, 164, 78, 14);
		frmEncuesta.getContentPane().add(lblIntroduzcaConstrasea);
		
		JLabel lblAficiones = new JLabel("Aficiones");
		lblAficiones.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAficiones.setBounds(611, 257, 102, 23);
		frmEncuesta.getContentPane().add(lblAficiones);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(611, 283, 141, 117);
		frmEncuesta.getContentPane().add(scrollPane_1);
		
		list = new JList();
		scrollPane_1.setViewportView(list);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"SALIR DE FIESTA", "VIAJES" , "SENDERISMO", "HACER DEPORTE", "PROGRAMAR JAVA", "LEER LIBROS", "CORRER", "IR AL GYM"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setToolTipText("Seleccione una aficcion");
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(591, 453, 216, 167);
		frmEncuesta.getContentPane().add(textArea);
		textArea.setLineWrap(true);
		
		JCalendar calendar = new JCalendar();
		calendar.getDayChooser().setBorder(new CompoundBorder());
		calendar.getDayChooser().getDayPanel().setBorder(new CompoundBorder());
		calendar.setBounds(591, 24, 184, 153);
		frmEncuesta.getContentPane().add(calendar);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd-MM-yy");
		dateChooser.setBounds(342, 24, 95, 20);
		frmEncuesta.getContentPane().add(dateChooser);
		
		JLabel lblElijaFecha = new JLabel("Elija fecha:");
		lblElijaFecha.setBounds(278, 24, 62, 20);
		frmEncuesta.getContentPane().add(lblElijaFecha);
		
		//Eventos
		txtIntroduzcaSuNombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				passwordField.setEnabled(true);
				System.out.println("Campo de texto: "+txtIntroduzcaSuNombre.getText()+1);

				
			}	
		});
	}
}
