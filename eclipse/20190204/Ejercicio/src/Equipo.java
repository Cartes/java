
public class Equipo {
	
	String nombre;
	int jugadores;
	double mediagoles;
	char tipo; // 'A' asociacion 'S' sociedad 'C' Club
	boolean ascenso;
	
	// Metodo Constructoras
	public Equipo(String nombre, int jugadores, double mediagoles, char tipo, boolean ascenso) {
		super();
		this.nombre = nombre;
		this.jugadores = jugadores;
		this.mediagoles = mediagoles;
		this.tipo = tipo;
		this.ascenso = ascenso;
	}
	
	
	//Metodo GET & SET
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getJugadores() {
		return jugadores;
	}

	public void setJugadores(int jugadores) {
		this.jugadores = jugadores;
	}

	public double getMediagoles() {
		return mediagoles;
	}

	public void setMediagoles(double mediagoles) {
		this.mediagoles = mediagoles;
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public boolean isAscenso() {
		return ascenso;
	}

	public void setAscenso(boolean ascenso) {
		this.ascenso = ascenso;
	}


	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", jugadores=" + jugadores + ", mediagoles=" + mediagoles + ", tipo=" + tipo
				+ ", ascenso=" + ascenso + "]";
	}
	
}

