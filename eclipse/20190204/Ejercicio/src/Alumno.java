
public class Alumno {
	
	String nombre;
	String apellidos;
	int nia;
	char sexo; // 'M' masculino y 'F' femenino
	double notaMedia;
	boolean Promociona;

	
	//Metodo Constructor
	public Alumno(String nombre, String apellidos, int nia, char sexo, double notaMedia, boolean promociona) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nia = nia;
		this.sexo = sexo;
		this.notaMedia = notaMedia;
		this.Promociona = promociona;
	}

	//Metodos GET & SET
	
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public int getNia() {
		return nia;
	}


	public void setNia(int nia) {
		this.nia = nia;
	}


	public char getSexo() {
		return sexo;
	}


	public void setSexo(char sexo) {
		this.sexo = sexo;
	}


	public double getNotaMedia() {
		return notaMedia;
	}


	public void setNotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}


	public boolean isPromociona() {
		return Promociona;
	}


	public void setPromociona(boolean promociona) {
		Promociona = promociona;
	}

	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + ", apellidos=" + apellidos + ", nia=" + nia + ", sexo=" + sexo
				+ ", notaMedia=" + notaMedia + ", Promociona=" + Promociona + "]";
	}
	
	
	
}
