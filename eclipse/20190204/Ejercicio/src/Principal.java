
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Alumno alumno1 = new Alumno("Mompo", "Soria", 3245452, 'M', 9.9, true);
		Alumno alumno2 = new Alumno("Diego", "Elvira", 3232213, 'M', 4.9, false);
		Alumno alumno3 = new Alumno("Alvaro", "Lago", 3211221, 'M', 4.9, true);
		Equipo equipo1 = new Equipo("RealDeVARdrid", 11, 0.5, 'C', false);
		System.out.println(alumno1);
		System.out.println(equipo1);

		alumno1.setNombre("Cristobal");
		System.out.println(alumno1.getNombre());

		System.out.println(equipo1.getNombre());

		Alumno[] arrayAlumnoArray = { alumno1, alumno2, alumno3 };

		for (int i = arrayAlumnoArray.length - 1; i >= 0; i--) {
			System.out.println(arrayAlumnoArray[i]);
		}

		int[][] matriz = { 
				{ 1, 3, 0, -2, 7 },
				{ 4, 9, 9, 5, 6 },
				{ 8, 1, 3, 4, 5 } 
				};

		// Primer for recorre filas
		for (int i = 0; i < matriz.length; i++) {
			// Segundo for recorre columnas
			for (int j = 0; j < matriz[i].length; j++) { //Matriz[i] es al principio la primera fila
				System.out.print(matriz[i][j]+"\t");
			}
			System.out.println();

		}
	}
}
